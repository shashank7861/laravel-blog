<?php
use App\Post;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'web'], function()
{
    Route::get('/', function () {
        $posts=Post::orderBy('created_at','asce')->get();
        return view('welcome',['posts'=>$posts]);
    });
    Route::post('/onsignup',[
        'uses'=>'UserController@postSignup',
        'as'=>'sign_up'
    ]);
    Route::post('/onsignin',[
        'uses'=>'UserController@postSignin',
        'as'=>'sign_in'
    ]);
    
    Route::get('/login',[
        'uses'=>'UserController@login',
        'as'=>'login'
        ]);
    Route::get('/logout',[
        'uses'=>'UserController@logout',
        'as'=>'logout'
        ]);    
    Route::get('/dashboard',[
        'uses'=>'PostController@dashboard',
        'as'=>'dashboard',
        'middleware'=>'auth'
    ]);
    Route::get('/signup','UserController@signup');
    Route::get('/users',[
        'uses'=>'UserController@users',
        'middleware'=>'auth'
        ]);
    Route::get('/del_users{id}',[
        'uses'=>'UserController@del_user',
        'as'=>'user.delete',
        'middleware'=>'auth'
    ]);
    Route::post('/postsubmit',[
        'uses'=>'PostController@postsubmit',
        'as'=>'post-submit'
    ]);
    Route::get('/viewposts',[
        'uses'=>'PostController@viewposts',
        'as'=>'viewposts',
        'middleware'=>'auth'
    ]);
    Route::get('/del_post{id}',[
        'uses'=>'PostController@del_post',
        'as'=>'post.delete',
        'middleware'=>'auth'
    ]);
    Route::post('/postedit',[
        'uses'=>'PostController@postEditPost',
        'as'=>'postedit',
        'middleware'=>'auth'
    ]);
    Route::get('/profile',[
        'uses'=>'UserController@profile',
        'as'=>'profile',
        'middleware'=>'auth'
    ]);
    Route::post('/updateprofile',[
        'uses'=>'UserController@postProfileSave',
        'as'=>'profilesave',
        'middleware'=>'auth'
    ]);
    Route::get('/profileimage/{filename}',[
        'uses'=>'UserController@getProfileImage',
        'as'=>'profileimage'
    ]);
    Route::get('/messenger',[
        'uses'=>'MessageContoller@messenger',
        'as'=>'messenger'
    ]);
    Route::post('/sendmsg',[
        'uses'=>'MessageContoller@sendmsg',
        'as'=>'send'
    ]);
});    