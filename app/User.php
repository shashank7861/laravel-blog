<?php

namespace App;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable;
    public function posts(){
        return $this->hasMany('App\Post');
    }
    public function msgs(){
        return $this->hasMany('App\Msg');
    }
    protected $fillable = ['email'];
 
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
 
    public function socialProfile()
    {
        return $this->hasOne(SocialLoginProfile::class);
    }
    
    
}
