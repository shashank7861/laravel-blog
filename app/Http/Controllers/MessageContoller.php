<?php

namespace App\Http\Controllers;

use App\User;
use App\Msg;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class MessageContoller extends Controller
{
    public function messenger(){
        return view('admin.messenger.messenger')->with(['msgs'=>Msg::all(),'user'=>Auth::user(),'users'=> User::all()]);
    }
    public function sendmsg(Request $request){
        $this->validate($request,[
            'msg'=>'required',
            'rec'=>'required'
        ]);
        $msg=new Msg();
        $sender=Auth::user()->id;
        $msg->sender_id=$sender;
        $msg->rec_id=$request['rec'];
        $msg->msg=$request['msg'];
        if($msg->save()){
            $msg="Message sent";
        }
        return view('admin.messenger.messenger')->with(['msgs'=>Msg::all(),'user'=>Auth::user(),'users'=> User::all()]);
    }
    public function showmsg(Request $request){

    }

}
