<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use Illuminate\Support\Facades\Auth;
class PostController extends Controller
{
    public function postsubmit(Request $request){
        $this->validate($request,[
            'content'=>'required',
            'title'=>'required'
        ]);
        $post=new Post();
        $post->title=$request['title'];
        $post->content=$request['content'];
        $msg="Erroc occured";
        if($request->user()->posts()->save($post)){
            $msg="Post Created";
        }
        $posts=Post::orderBy('created_at','asce')->get();
        return view('admin.dashboard',['msg'=>$msg,'posts'=>$posts,'user'=>Auth::user()]);
    }
    public function viewposts(){
        return view('admin.post.posts')->with(['posts'=>Post::all(),'user'=>Auth::user()]);
    }
    public function del_post($id)
    {   
        $post = Post::where('id',$id)->first();
        if(Auth::user()!=$post->user){
            return redirect()->back();
        }
        $post->delete();
        return Redirect('/viewposts');
    }
    public function dashboard(){
        $posts=Post::orderBy('created_at','asce')->get();
        return view('admin.dashboard',['posts'=>$posts,'user'=>Auth::user()]);
    }
    public function postEditPost(Request $request){
        $this->validate($request,[
            'title'=>'required',
            'content'=>'required',
            'id'=>'required'
        ]);
        $postid=$request['id'];
        $post = Post::where('id',$postid)->first();
        $post->title=$request['title'];
        $post->content=$request['content'];
        $post->update();
        return redirect()->back();
    }        
}
