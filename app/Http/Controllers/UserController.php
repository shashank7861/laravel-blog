<?php

namespace App\Http\Controllers;
use App\User;
use App\Post;
//use Illuminate\Mail\Mailer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function postSignup(Request $request){
        $this->validate($request,[
            'email'=>'required|email|unique:users',
            'name'=>'required',
            'password'=>'required|min:6'
        ]);

        $name=$request['name'];
        $email=$request['email'];
        $password=bcrypt($request['password']);

        $user=new User;
        $user->email=$email;
        $user->name=$name;
        $user->password=$password;

        $user->save();
        Auth::login($user);
        $user = User::where('email',$request->email)->first();
        session(['user' => $user]);
        $request->session()->flash('message', 'User created succesfully!'); 
        /*Mail::send('contact.display',$data, function($message)
        {
            $message->to('shashank.shekhar@laitkor.com')->subject('Welcome!');
        });*/
        return redirect('/dashboard');
    }
    public function postSignin(Request $request){
        
        $this->validate($request,[
            'email'=>'required',
            'password'=>'required'
        ]);

        if(Auth::attempt(['email'=>$request['email'],'password'=>$request['password']])){
            $user = User::where('email',$request->email)->first();
            session(['user' => $user]);
            return redirect('/dashboard');
        }
        else{
            $request->session()->flash('message', 'Please check Email/Password');         
            return redirect()->back();
        }
    }
    public function signup(){
        return view('signup');
    }
    public function login(){
        return view('login');
    }
    public function users()
    {
        return view('admin.users')->with(['users'=> User::all(),'user'=>Auth::user()]);
    }
    public function profile()
    {
        return view('admin.account',['user'=>Auth::user()]);
    }
    public function del_user($id)
    {   
        $user = User::where('id',$id)->first();
        $user->delete();
        //Session::flash('message', 'Successfully deleted the user!');
        return Redirect('users');
    }
    public function logout()
    {
        Auth::logout();
        session(['user' => '']);
        return redirect('login');
    }
    public function postProfileSave(Request $request){
        $this->validate($request,[
            'name'=>'required',
            // 'image'=>'required'
        ]);
        $user=Auth::user();
        $user->name=$request['name'];
        $user->update();
        $file=$request->file('image');
        $filename=$user->id.'.jpg';
        if($file){
            //Storage::disk('local')->put($filename, $file);
            Storage::disk('local')->put($filename,File::get($file));    
        }
        return Redirect('profile');
    }
    public function getProfileImage($filename){
        $file=Storage::disk('local')->get($filename);
        return new Response($file);
    }
    
}
