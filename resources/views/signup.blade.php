@extends('layouts.master')
@section('content')    
<div class="row well">
    <div class="col-md-6 col-md-offset-3">
        <h1>Log In</h1><hr />
        @include('parts.msg-block')
        <form class="form-horizontal" action="{{ route('sign_up')}}" method="post">
            <fieldset>
                <div class="form-group {{$errors->has('name')? 'has-error':''}}">
                    <label for="inputName" class="col-lg-2 control-label">Name</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ Request::old('name') }}">
                    </div>
                </div>
                <div class="form-group {{$errors->has('email')? 'has-error':''}}">
                    <label for="inputEmail" class="col-lg-2 control-label">Emailz</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="{{ Request::old('email') }}">
                    </div>
                </div>
                <div class="form-group {{$errors->has('password')? 'has-error':''}}">
                    <label for="inputPassword" class="col-lg-2 control-label">Password</label>
                    <div class="col-lg-10">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                <div class="col-lg-4">
                    <button style="float:right" type="submit" class="btn btn-success">Sign Up</button>
                </div>
                </div>
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </fieldset>
        </form>
    </div>
</div>  
@endsection    