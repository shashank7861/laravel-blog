<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Blog</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav">
        @if(!session('user'))
        <li><a href="{{ url('/') }}">Home</a></li>
        @endif
        <li><a href="{{url('/dashboard')}}">Dashboard</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Operations <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{{url('/users')}}">View Users</a></li>
            <li><a href="{{url('/viewposts')}}">View Post</a></li>
            
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      @if(!session('user'))
          <li><a href="{{ url('/signup') }}">Sign Up</a></li>
          <li><a href="{{ url('/login') }}">Log In</a></li>
          @else
          <li><a href="{{url('/profile')}}">{{session('user')->name}}</a></li>
          <li><a href="{{ url('/logout') }}">Log Out</a></li>
      @endif
      </ul>
    </div>
  </div>
</nav>