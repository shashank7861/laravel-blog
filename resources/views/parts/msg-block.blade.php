@if(count($errors)>0)
    <div class="row">
        <div class"col-md-8">
            <ul class="list-group">
                @foreach($errors->all() as $error)
                <li class="list-group-item list-group-item-danger">{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

@if(Session::has('msg'))
    <div class="row">
        <div class"col-md-8">
            {{Session::get('msg')}}
        </div>
    </div>
@endif