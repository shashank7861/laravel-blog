@extends('layouts.master')
@section('content')
<div class="row well">
    <div class="col-md-6 col-md-offset-3">
        <h1>Log In</h1><hr />
        @include('parts.msg-block')
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('message') }}</p>
        @endif
        <form class="form-horizontal" method="post" action="{{route('sign_in')}}">
            <fieldset>
                <div class="form-group {{$errors->has('email')? 'has-error':''}}">
                <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="email" id="inputEmail" placeholder="Email">
                </div>
                </div>
                <div class="form-group {{$errors->has('password')? 'has-error':''}}">
                    <label for="inputEmail" class="col-lg-2 control-label">Password</label>
                    <div class="col-lg-10">
                    <input type="password" class="form-control" name="password" id="inputPassword" placeholder="Password">
                    </div>
                </div>    
                <div class="form-group">
                <div class="col-lg-4">
                    <button type="submit" style="float:right" class="btn btn-success">Log In</button>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>  
@endsection    