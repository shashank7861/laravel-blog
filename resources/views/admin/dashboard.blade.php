<style>
.message-item {
margin-bottom: 25px;
margin-left: 40px;
position: relative;
}
.message-item .message-inner {
background: #fff;
border: 1px solid #ddd;
border-radius: 3px;
padding: 10px;
position: relative;
}
.message-item .message-inner:before {
border-right: 10px solid #ddd;
border-style: solid;
border-width: 10px;
color: rgba(0,0,0,0);
content: "";
display: block;
height: 0;
position: absolute;
left: -20px;
top: 6px;
width: 0;
}
.message-item .message-inner:after {
border-right: 10px solid #fff;
border-style: solid;
border-width: 10px;
color: rgba(0,0,0,0);
content: "";
display: block;
height: 0;
position: absolute;
left: -18px;
top: 6px;
width: 0;
}
.message-item:before {
background: #fff;
border-radius: 2px;
bottom: -30px;
box-shadow: 0 0 3px rgba(0,0,0,0.2);
content: "";
height: 100%;
left: -30px;
position: absolute;
width: 3px;
}
.message-item:after {
background: #fff;
border: 2px solid #ccc;
border-radius: 50%;
box-shadow: 0 0 5px rgba(0,0,0,0.1);
content: "";
height: 15px;
left: -36px;
position: absolute;
top: 10px;
width: 15px;
}
.clearfix:before, .clearfix:after {
content: " ";
display: table;
}
.message-item .message-head {
border-bottom: 1px solid #eee;
margin-bottom: 8px;
padding-bottom: 8px;
}
.message-item .message-head .avatar {
margin-right: 20px;
}
.message-item .message-head .user-detail {
overflow: hidden;
}
.message-item .message-head .user-detail h5 {
font-size: 16px;
font-weight: bold;
margin: 0;
}
.message-item .message-head .post-meta {
float: left;
padding: 0 15px 0 0;
}
.message-item .message-head .post-meta >div {
color: #333;
font-weight: bold;
text-align: right;
}
.post-meta > div {
color: #777;
font-size: 12px;
line-height: 22px;
}
.message-item .message-head .post-meta >div {
color: #333;
font-weight: bold;
text-align: right;
}
.post-meta > div {
color: #777;
font-size: 12px;
line-height: 22px;
}
.img1 {
 min-height: 40px;
 max-height: 40px;
}


</style>
@extends('admin.layout')
@section('title', 'Dashboard')
@section('content')
<h1>Dashboard</h1>
<span style="float:right">Welcome : <b>{{session('user')->name}}</b></span>
<div class="row">
    <h2><i class="fa fa-comments"></i> Posts</h2>      
    <div class="col-md-12">
        @include('parts.msg-block')
        <form class="form" method="post" action="{{route('post-submit')}}">
            <fieldset>
                <div class="form-group col-md-5 {{$errors->has('title')? 'has-error':''}}">
                <label for="inputTitle" class="col-lg-2 control-label">Title</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="title" id="inputTitle" placeholder="Title">
                </div>
                </div>
                <div class="form-group col-md-6 {{$errors->has('content')? 'has-error':''}}">
                    <label for="inputContent" class="col-lg-2 control-label">Content</label>
                    <div class="col-lg-10">
                    <textarea class="form-control" name="content"  placeholder="Content"></textarea>
                    </div>
                </div>    
                <div class="form-group">
                <div class="col-lg-1">
                    <button type="submit" style="float:right" class="btn btn-success">Post</button>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
  <div class="row">
    <div class="col-md-12">
      <div class="qa-message-list" id="wallmessages">
        @foreach ($posts as $post)
    		<div class="message-item" id="m16">
						<div class="message-inner">
							<div class="message-head clearfix">
								<div class="avatar pull-left"><a href="#"><img src="{{route('profileimage',['filename'=>$post->user->id.'.jpg'])}}" class="img img-circle" style="max-width:50px;min-height:50px;"></a></div>
								<div class="user-detail">
									<h5 class="handle">{{$post->title}}</h5>
									<div class="post-meta">
										<div class="asker-meta">
											<span class="qa-message-what"></span>
											<span class="qa-message-when">
												<span class="qa-message-when-data"></span>
											</span>
											<span class="qa-message-who">
												<span class="qa-message-who-pad">by </span>
												<span class="qa-message-who-data"><a href="#">{{$post->user->name}}</a></span>
                        <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> {{$post->created_at}}</time>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="qa-message-content">
								<p class="text-justify">{{$post->content}}</p>
							</div>
					  </div>
          </div>
          @endforeach
          </div>
   </div> 
</div>
@endsection
