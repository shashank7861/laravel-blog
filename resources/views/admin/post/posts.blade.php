@extends('admin.layout')
@section('content')
<h1>Posts</h1>
<table id="user-table" class="table table-hover">
<thead>
<tr><th>Post Id</th><th>User Name</th><th>Title</th><th>Content</th><th>Status</th><th>Options</th></tr>
</thead>
@foreach ($posts as $post)
    @if(Auth::user()==$post->user)
    <tr id="post" data-postid="{{ $post->id }}"><td>{{$post->id}}</td><td>{{$post->user->name}}</td><td>{{$post->title}}</td><td><p class="text-justify">{{$post->content}}</p></td><td>{{$post->status}}</td>
        <td class="col-sm-2" id="contenttoedit">
            <a href="#" id="editpost{{$post->id}}" class="editpost"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a> 
            <a href="{{route('post.delete',['id'=>$post->id])}}"><button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button></a>
        </td>
    </tr>
    @endif
@endforeach
</table>
<div class="modal fade" id="edit_post" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Post</h4>
      </div>
      <form class="form-horizontal" method="post" action="{{route('postedit')}}">
      <div class="modal-body">
        @include('parts.msg-block')
        <form class="form-horizontal" method="post" action="{{route('postedit')}}">
            <fieldset>
                <div class="form-group{{$errors->has('title')? 'has-error':''}}">
                <label for="inputTitle" class="col-lg-2 control-label">Post Id</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="id" readonly id="id" placeholder="Title">
                </div>
                </div>
                <div class="form-group{{$errors->has('title')? 'has-error':''}}">
                <label for="inputTitle" class="col-lg-2 control-label">Title</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="title" id="title" placeholder="Title">
                </div>
                </div>
                <div class="form-group{{$errors->has('content')? 'has-error':''}}">
                    <label for="inputContent" class="col-lg-2 control-label">Content</label>
                    <div class="col-lg-10">
                    <textarea class="form-control" name="content" id="content" placeholder="Content"></textarea>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    </div>
                </div>    
                <div class="form-group">
                </div>
            </fieldset>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
