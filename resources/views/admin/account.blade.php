@extends('admin.layout')
@section('title', '{{$user->name}}')
@section('content')
<h1>User Profile</h1>
<section class="row">
        <div class="col-md-8">
            <form class="form-horizontal" method="post" action="{{route('profilesave')}}" enctype="multipart/form-data">
                <fieldset>
                    <div class="form-group col-md-12 {{$errors->has('name')? 'has-error':''}}">
                        <label for="inputTitle" class="col-lg-3 control-label">Name</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" value="{{$user->name}}" name="name" id="name" placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group col-md-12 {{$errors->has('email')? 'has-error':''}}">
                        <label for="inputTitle" class="col-lg-3 control-label">Email</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="email" value="{{$user->email}}" id="email" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group col-md-12 {{$errors->has('image')? 'has-error':''}}">
                        <label for="inputContent" class="col-lg-3 control-label">Profile Image</label>
                        <div class="col-lg-9">
                            <input type="file" class="form-control" name="image" id="image" />
                        </div>
                    </div>    
                    <div class="form-group">
                    <div class="col-lg-11">
                        <button type="submit" style="float:right" class="btn btn-success">Update</button>
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                    </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="col-md-4">
          @if (Storage::disk('local')->has($user->id.'.jpg'))
          <section>
            <img src="{{route('profileimage',['filename'=>$user->id.'.jpg'])}}" class="img img-responsive">
          </section>
          @endif  
        </div>
</section>
@endsection