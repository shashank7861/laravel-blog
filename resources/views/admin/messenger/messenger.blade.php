@extends('admin.layout')
@section('content')
<h1>Messenger</h1>
<section class="row">
        <div class="col-md-3">
            <div class="list-group">
                @foreach ($users as $usera)
                    <button id="username" type="button" class="list-group-item">{{$usera->name}}</button>
                    <a href="#msg" data-toggle="collapse"><button id="user-name" type="button" class="list-group-item">{{$usera->name}}</button></a>
                @endforeach
            </div>
        </div>
        <div class="col-md-9 well">
            <label for="inputTitle" id="lbl_user" class="control-label">Message</label><hr>
            <div id="msg" class="collaspe-panel">
                <table class="table table-bordered">
                    <tr><th>Rec</th><th>Message</th><th>Sender</th></tr>
                    @foreach ($msgs as $msga)
                    <tr><td>    
                        <?php 
                        if($msga->rec_id)
                            $rec = App\User::where('id',$msga->rec_id)->first();
                            echo $rec->name;
                        ?>
                        </td><td>{{$msga->msg}}</td>
                        <td>
                        <?php 
                        if($msga->sender_id)
                            $sender = App\User::where('id',$msga->sender_id)->first();
                            echo $sender->name;
                        ?>
                        </td>
                    @endforeach
                </table>
                <form class="form" method="post" action="{{route('send')}}">
                <fieldset>
                    <div class="col-md-12">
                        <div class="form-group col-lg-10">
                            <select class="form-control" name="rec">
                                <option value="0">Select User</option>
                                @foreach ($users as $usera)
                                    @if($usera->id!=$user->id)
                                    <option value="{{$usera->id}}">{{$usera->name}}</option>
                                    @endif
                                @endforeach    
                            </select>
                        </div>
                        <div class="form-group col-lg-10">
                            <textarea class="form-control" row="10" style="resize:none;" cols="10" name="msg" id="name" placeholder="Type Your message here"></textarea>
                            <input type="hidden" name="_token" value="{{ Session::token() }}">
                        </div>
                        <div class="form-group col-lg-2">
                            <button type="submit" style="float:right" class="btn btn-success"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</button>
                        </div>
                    </div>
                </fieldset>
            </form>
            </div>            
        </div>
</section>
@endsection