@extends('admin.layout')
@section('content')
<h1>Users</h1>

<table id="user-table" class="table table-hover">
<thead>
<tr><th>User Id</th><th>Name</th><th>Email</th><th>Options</th></tr>
</thead>
@foreach ($users as $usera)
    <tr><td>{{$usera->id}}</td><td>{{$usera->name}}</td><td>{{$usera->email}}</td>
        <td class="col-sm-2"><a href="{{route('user.delete',['id'=>$user->id])}}"><button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button></a>
        </td>
    </tr>
@endforeach
</table>
@endsection